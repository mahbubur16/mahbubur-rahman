import "./Office.css";
import { TbTie } from "react-icons/tb";
import React from 'react'
import { HiOfficeBuilding } from "react-icons/hi";
import { FaCalendarAlt } from "react-icons/fa";

const Office = () => {
  return (
    <div className="office">
        <div className="exp">
            <h2>Experiences</h2>
        </div> 
        <div className="prof">
            <p className="ofc"><HiOfficeBuilding /> RTC Hubs Ltd.</p>
            <br />
            <p><TbTie /> Software Developer</p>
            <br />
            <p><FaCalendarAlt /> Mar 2023 - Apr 2024</p>

            <br />
            <br />
            <br />

            <p className="ofc"><HiOfficeBuilding /> DataTechGen</p>
            <br />
            <p><TbTie /> Data Analyst</p>
            <br />
            <p><FaCalendarAlt /> May 2024 - Sep 2024</p>
        </div>
    </div>
  )
}

export default Office